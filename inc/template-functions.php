<?php
/**
 * Created by PhpStorm.
 * User: Quan
 * Date: 11/22/2017
 * Time: 10:55 AM
 */

function je_template_job_loop_summary_title() {
	global $post;

	$status   = '';
	$featured = get_post_meta( $post->ID, '_featured', true );

	if ( $featured ) {
		$status = array( 'id' => 'featured', 'name' => esc_html__( 'Featured', 'jobboard' ) );
	}

	JB_Event()->get_template( 'loop/title.php', array( 'status' => $status ) );
}

function je_template_job_loop_summary_duration() {
	global $post;

	$start      = get_post_meta( $post->ID, '_start', true );
	$end        = get_post_meta( $post->ID, '_end', true );
	$date_start = date( 'F, d Y', strtotime( $start ) );
	$date_end   = date( 'F, d Y', strtotime( $end ) );
	if ( $date_start === $date_end ) {
		$date = date( 'F, d Y', strtotime( $start ) );
		$time = date( 'h:i A', strtotime( $start ) ) . ' - ' . date( 'h:i A', strtotime( $end ) );
		JB_Event()->get_template( 'loop/duration.php', array( 'date' => $date, 'time' => $time ) );
	} else {
		$date = date( 'F, d Y h:i A', strtotime( $start ) ) . ' - ' . date( 'F, d Y h:i A', strtotime( $end ) );
		JB_Event()->get_template( 'loop/duration.php', array( 'date' => $date ) );
	}
}

function je_template_job_loop_summary_location() {
	JB_Event()->get_template( 'loop/location.php' );
}

function je_template_event_loop_actions_readmore() {
	JB_Event()->get_template( 'loop/readmore.php' );
}

function je_template_event_loop_actions() {
	JB_Event()->get_template( 'loop/actions.php' );
}

function je_template_single_header() {
	JB_Event()->get_template( 'single/header.php' );
}

function je_template_single_register() {
	JB_Event()->get_template( 'single/register.php' );
}

function je_template_single_summary() {
	the_content();
}

function je_template_single_map() {
	if ( function_exists( 'jb_map' ) ) {
		JB_Event()->get_template( 'single/map.php' );
	}
}

function je_template_loop_start() {
	JB_Event()->get_template( 'loop/loop-start.php' );
}

function je_template_loop_end() {
	JB_Event()->get_template( 'loop/loop-end.php' );
}

function je_template_job_loop_summary_start() {
	JB_Event()->get_template( 'loop/summary-start.php' );
}

function je_template_job_loop_summary_end() {
	JB_Event()->get_template( 'loop/summary-end.php' );
}
