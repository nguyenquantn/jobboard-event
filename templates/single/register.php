<?php
/**
 * Created by PhpStorm.
 * User: Quan
 * Date: 11/28/2017
 * Time: 8:57 AM
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<div class="register-event">
    <button id="register-event"><?php esc_html_e( 'Register Event', 'jobboard-event' ); ?></button>
    <div class="register-event-popup" style="display: none;">
        <div class="header">
            <h2><?php esc_html_e( 'Register Event', 'jobboard-event' ); ?></h2>
            <p><?php esc_html_e( 'Thanks for showing interest in the event below. Fill out the form to register your interest in our event and for further information.', 'jobboard-event' ); ?></p>
        </div>
        <div class="body">
            <div class="post-event">
                <h1 class="entry-title"><?php the_title(); ?></h1>
                <div class="entry-meta">
					<?php do_action( 'jobboard_event_single_header_meta' ); ?>
                </div>
            </div>

            <form method="post">
                <input type="text" name="fullname" placeholder="<?php esc_html_e( 'Full Name*', 'jobboard-event' ); ?>"
                       required>
                <input type="email" name="email"
                       placeholder="<?php esc_html_e( 'Email Address*', 'jobboard-event' ); ?>" required>
                <input type="tel" name="phone" placeholder="<?php esc_html_e( 'Contact Number', 'jobboard-event' ); ?>">
                <button type="submit"><?php esc_html_e( 'Register Event', 'jobboard-event' ); ?></button>
            </form>
        </div>
    </div>
</div>
