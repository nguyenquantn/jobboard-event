<?php
/**
 * Created by PhpStorm.
 * User: Quan
 * Date: 11/22/2017
 * Time: 8:27 AM
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit();
}
if ( ! class_exists( 'JB_Event_Post' ) ) {
	class JB_Event_Post {
		public function __construct() {
			add_action( 'init', array( $this, 'add_event_post' ) );
		}

		public function add_event_post() {
			global $redux_meta;
			$page_event   = jb_get_option( 'page-events' );
			$event_labels = array(
				'name'           => esc_html__( 'Events', 'jobboard-event' ),
				'singular_name'  => esc_html__( 'Events', 'jobboard-event' ),
				'menu_name'      => esc_html__( 'JobBoard Event', 'jobboard-event' ),
				'name_admin_bar' => esc_html__( 'Events', 'jobboard-event' ),
				'add_new'        => esc_html__( 'New', 'jobboard-event' ),
				'add_new_item'   => esc_html__( 'New Event', 'jobboard-event' ),
				'edit_item'      => esc_html__( 'Edit Event', 'jobboard-event' ),
				'all_items'      => esc_html__( 'All', 'jobboard-event' )
			);

			$event = array(
				'menu_icon' => 'dashicons-pressthis',
				'labels'    => $event_labels,
				'show_ui'   => true,
				'public'    => true,
				'supports'  => array( 'title', 'editor' ),
			);

			if ( $page_event && get_post( $page_event ) ) {
				$event['has_archive'] = get_page_uri( $page_event );
			}

			$event_type_labels = array(
				'name'              => esc_html__( 'Types', 'jobboard-event' ),
				'singular_name'     => esc_html__( 'Type', 'jobboard-event' ),
				'search_items'      => esc_html__( 'Search Types', 'jobboard-event' ),
				'all_items'         => esc_html__( 'All Types', 'jobboard-event' ),
				'parent_item'       => esc_html__( 'Parent Type', 'jobboard-event' ),
				'parent_item_colon' => esc_html__( 'Parent Type:', 'jobboard-event' ),
				'edit_item'         => esc_html__( 'Edit Type', 'jobboard-event' ),
				'update_item'       => esc_html__( 'Update Type', 'jobboard-event' ),
				'add_new_item'      => esc_html__( 'Add New Type', 'jobboard-event' ),
				'new_item_name'     => esc_html__( 'New Type', 'jobboard-event' ),
				'menu_name'         => esc_html__( 'Types', 'jobboard-event' ),
			);

			$event_type = array(
				'hierarchical'       => true,
				'labels'             => $event_type_labels,
				'show_ui'            => true,
				'show_admin_column'  => true,
				'query_var'          => true,
				'show_in_quick_edit' => true,
				'rewrite'            => array(
					'slug' => jb_get_option( 'taxonomy-event-type-slug', 'event-type' )
				),
			);

			register_post_type( 'jb-events', $event );
			register_taxonomy( 'jobboard-event-type', array( 'jb-events' ), apply_filters( 'jb/taxonomy/event_type/args', $event_type ) );

			$setting                  = JB()->post->post_args();
			$setting['open_expanded'] = true;
			$redux_meta->post->add( $setting, $this->sections_event(), 'event-edit', esc_html__( 'Edit', 'jobboard-event' ), 'jb-events' );
		}

		public function sections_event() {
			$sections = array(
				'setting' => array(
					'title'  => '',
					'id'     => 'event-meta',
					'fields' => array(
						array(
							'id'       => '_start',
							'type'     => 'rc_datetime',
							'title'    => esc_html__( 'Start', 'jobboard-event' ),
							'subtitle' => esc_html__( 'Choose datetime.', 'jobboard-event' )
						),
						array(
							'id'       => '_end',
							'type'     => 'rc_datetime',
							'title'    => esc_html__( 'End', 'jobboard-event' ),
							'subtitle' => esc_html__( 'Choose datetime.', 'jobboard-event' )
						),
						array(
							'id'       => '_address',
							'type'     => 'textarea',
							'title'    => esc_html__( 'Complete Address', 'jobboard-event' ),
							'subtitle' => esc_html__( 'Enter you complete address with city, state or country.', 'jobboard-event' ),
							'default'  => '',
						)
					)
				)
			);

			return apply_filters( 'jobboard_event_sections', $sections );
		}

	}
}